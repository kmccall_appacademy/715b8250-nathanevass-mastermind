class Code
  attr_reader :pegs
    PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "P" => :purple,
    "R" => :red,
    "Y" => :yellow
  }
  def initialize(pegs)
    @pegs = pegs 
  end 
  
  def [](n)
    @pegs[n]
  end 
  
  def self.random
    code_arr = []
    4.times {code_arr << PEGS.values.sample}
    Code.new(code_arr)
  end 
  
  def self.parse(string)
    parsed_arr = string.split("").map do |ch|
      ch.upcase! 
      raise "error parsing color" unless PEGS.keys.include?(ch)
      PEGS[ch]
    end 
    Code.new(parsed_arr)
  end 
  
  def exact_matches(code2)
    matches = 0
    pegs.each_with_index do |peg, i|
      matches += 1 if peg == code2[i]
    end 
    matches 
  end 
  
  def ==(code2)
    return false unless code2.is_a? Code 
    pegs == code2.pegs
  end 
  
  def near_matches(code2)
    matches = {}
    pegs.each_with_index do |peg, i|
      matches[peg] = true if code2.pegs.include?(peg)
    end     
    matches.length - exact_matches(code2)
  end 
  
end

class Game
  attr_reader :secret_code
  
  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end 
  
  def play 
    5.times do 
      guess = get_guess
      if guess == @secret_code
        puts "you got it!"
        return 
      else 
        display_matches(guess)
      end 
    end 
    puts "you lose"
  end 
  
  def get_guess
    puts "enter a code ex 'RGBY'"
    guess_string = gets.chomp 
    Code.parse(guess_string)
  end 
  
  def display_matches(guess)
    puts "You got #{@secret_code.exact_matches(guess)} exact matches"
    puts "You got #{@secret_code.near_matches(guess)} near matches"
  end 
end

if __FILE__ == $PROGRAM_NAME
  Game.new.play
end
